package salt

import (
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {
	m := make(map[string]bool)
	for x := 1; x < 32; x++ {
		s := New()
		if m[string(s)] {
			t.Errorf("New() returned duplicated salt")
		}
		l := len(s)
		if l != 32 {
			t.Errorf("New() returned salt with length %d. Must 32", l)
		}
	}
}

func TestMust(t *testing.T) {
	s := []byte{1, 2, 3}
	w := []byte{1, 2, 3}
	if got := Must(s, nil); !reflect.DeepEqual(got, w) {
		t.Errorf("Must() = %v, want %v", got, w)
	}
}