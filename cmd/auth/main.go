package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
)

func main () {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)

	// Инициализация подсистем

	wg := &sync.WaitGroup{
	}
	wg.Add(1)

	// Запуск


	<-ctx.Done()
	cancel()
	wg.Wait()
}
